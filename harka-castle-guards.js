let missingTile, guards, isLazy, isLazy2, log;
const startButton = document.querySelector('button');
const setup = document.querySelector('article');
const game = document.querySelector('aside');
const positions = [];
const letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G'];
const numbers = [1, 2, 3, 4, 5, 6, 7];
const randomNumber = () => Math.floor(Math.random() * 7);
const nextTile = () => `${letters[randomNumber()]}${numbers[randomNumber()]}`;
const getSelectedValue = (name) =>
  document.querySelector(`select[name="${name}"]`).value;

const start = () => {
  setup.hidden = true;
  missingTile = getSelectedValue('x') + getSelectedValue('y');
  guards = getSelectedValue('guards');
  isLazy = !!getSelectedValue('isLazy');
  isLazy2 = !!getSelectedValue('isLazy2');
  let html = '<h2>Game</h2>';
  for (let i = 0; i < guards; i++)
    html += `<button id="${i}">Move guard ${i + 1}</button>`;
  game.innerHTML = `${html}<div></div>`;
  log = document.querySelector('div');
  game.hidden = false;
};

const addLog = (text) =>
  log.prepend(
    Object.assign(document.createElement('div'), { textContent: text })
  );

const move = (guardNumber) => {
  const targetTile = nextTile();
  if (
    (targetTile === missingTile && isLazy) ||
    (targetTile === positions[guardNumber] && isLazy2)
  ) {
    return addLog(`Guard ${guardNumber + 1} takes a break.`);
  }
  if (
    (targetTile === missingTile && !isLazy) ||
    (targetTile === positions[guardNumber] && !isLazy2)
  ) {
    return move(guardNumber);
  }
  positions[guardNumber] = targetTile;
  addLog(`Guard ${guardNumber + 1} target is ${targetTile}.`);
};

document.onclick = (e) => {
  if (e.target.tagName !== 'BUTTON') return;
  e.target.id === 'start' ? start() : move(parseInt(e.target.id));
};

if ('serviceWorker' in navigator)
  window.addEventListener('load', () =>
    navigator.serviceWorker.register('/worker.js').catch((err) => {})
  );
