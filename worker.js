const assets = [
  '/',
  '/style.css',
  '/script.js',
  '/harka-castle-guards',
  '/harka-castle-guards.js',
  '/harka-castle-rules',
  '/img/harka-castle.jpg',
  '/favicon.ico',
  '/img/site.webmanifest',
];

self.addEventListener('fetch', (event) => {
  event.respondWith(
    caches.match(new URL(event.request.url).pathname).then((cachedResponse) => {
      if (cachedResponse) {
        if (navigator.onLine) {
          fetch(event.request).then((networkResponse) => {
            caches
              .open('cache')
              .then((cache) =>
                cache.put(event.request, networkResponse.clone())
              );
          });
        }
        return cachedResponse;
      }
      return new Response('You are offline, and this resource is not cached.', {
        status: 503,
        statusText: 'Service Unavailable',
      });
    })
  );
});

self.addEventListener('install', (event) =>
  event.waitUntil(caches.open('cache').then((cache) => cache.addAll(assets)))
);
